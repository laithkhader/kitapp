﻿using AFOC_APP.Models;
using SAP.Middleware.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AFOC_APP
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            BackEndConfig config = new BackEndConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(config);


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
                AFOCDBContext con = new AFOCDBContext();
            try
            {
                con.Database.Initialize(false);

            }
            catch (Exception)
            {
            }
            con.Database.CreateIfNotExists();

            PRDBContext con2 = new PRDBContext();
            con2.Database.Initialize(true);
            con2.Database.CreateIfNotExists();


        }
    }
}
