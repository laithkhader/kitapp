﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AFOC_APP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
                name: "default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
             name: "Admin",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Admin", action = "Panel", id = UrlParameter.Optional }
         );
            routes.MapRoute(
                name: "Create",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Create", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Edit",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Edit", action = "Edit", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Search",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Edit", action = "Search", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                 name: "Report",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Report", action = "Report", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                 name: "Release",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Release", action = "Release", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                  name: "Reset",
                  url: "{controller}/{action}/{id}",
                  defaults: new { controller = "Reset", action = "Reset", id = UrlParameter.Optional }
              );
            routes.MapRoute(
                name: "Track",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Track", action = "Track", id = UrlParameter.Optional }
  );
        }
    }
}
