﻿using AFOC_APP.Models;
using SAP.Middleware.Connector;
using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;


namespace AFOC_APP.Controllers
{
    public class UserCheck : Controller
    {
       static RfcDestination prdc;
        bool disregflag;
        public UserCheck()
        {
            disregflag = false;
    
        }

        public static Boolean CheckExistance(string Username ,string password){
            
          
            
            var returnMsg = new System.Text.StringBuilder();
            //BackEndConfig config = new BackEndConfig(Username, password);
            try
            {
                
                prdc = RfcDestinationManager.GetDestination("AFOCH");
               

                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //prd.User = Username;
                //prd.Password = password;
                


                RfcRepository repo = prd.Repository;

                IRfcFunction UaerCheck = repo.CreateFunction("BAPI_USER_EXISTENCE_CHECK");
                UaerCheck.SetValue("USERNAME", Username);
                UaerCheck.Invoke(prd);



                var RETURN = UaerCheck["RETURN"].GetStructure();
                 

             
             
                    returnMsg.AppendLine(RETURN.GetString("MESSAGE"));
                
//RfcSessionManager.EndContext(prd);
               

            }
            catch (RfcCommunicationException e)
            {
                return false;

                //pr.Master.SapErrorMSG = " network problem...";
                //db.Masters.Attach(pr.Master.SapErrorMSG);
                //db.Entry(pr).State = EntityState.Modified;
                //db.SaveChanges();
                //var master = new Master();
                //master.SapErrorMSG = " network problem...";
                //return View("ErrorPage", master);
            }
            catch (RfcLogonException e)
            {
                return false;

                //res.SapErrorMSG = " user could not logon...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                //var master = new Master();
                //master.SapErrorMSG = " user could not logon...";
                //return View("ErrorPage", master);
                // user could not logon...
            }
            catch (RfcAbapRuntimeException e)
            {
                return false;

                //res.SapErrorMSG = " serious problem on ABAP system side...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                // serious problem on ABAP system side...
                //var master = new Master();
                //master.SapErrorMSG = " serious problem on ABAP system side...";
                //return View("ErrorPage", master);
            }
            catch (RfcAbapBaseException e)
            {
                return false;

                //var master = new Master();
                //master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                //return View("ErrorPage", master);
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
            }
            catch (Exception e)
            {
                throw e;



                //var master = new Master();
                //master.SapErrorMSG = e.Message;
                //return View("ErrorPage", master);
            }
            finally
            {
                //RfcDestinationManager.UnregisterDestinationConfiguration(config);
            }


            var s = returnMsg.ToString().Trim();

            var s2 = "User " + Username + " does not exist";
            s2.Trim();

            s.ToUpper();
            s2.ToUpper();

            if (s.Equals(s2))

            return false;

            else
            {
                return true;
            }
            
            
           }



        public static Boolean Validate(string Username, string Password) {


            var returnMsg = new System.Text.StringBuilder();

          

            try
            {
                prdc = RfcDestinationManager.GetDestination("AFOCH");


                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //prd.User = Username;
                //prd.Password = Password;


                RfcRepository repo = prd.Repository;

                IRfcFunction UaerCheck = repo.CreateFunction("SUSR_CHECK_LOGON_DATA");
                UaerCheck.SetValue("AUTH_METHOD", "P");

                UaerCheck.SetValue("USERID", Username);
                UaerCheck.SetValue("PASSWORD", Password);
                UaerCheck.Invoke(prd);



                var RETURN = UaerCheck[4].GetString();


                var returnID = "";
                returnMsg.AppendLine(RETURN);
                
//RfcSessionManager.EndContext(prd);

            }
            catch (RfcCommunicationException e)
            {
                throw e;
                //pr.Master.SapErrorMSG = " network problem...";
                //db.Masters.Attach(pr.Master.SapErrorMSG);
                //db.Entry(pr).State = EntityState.Modified;
                //db.SaveChanges();
                //var master = new Master();
                //master.SapErrorMSG = " network problem...";
                //return View("ErrorPage", master);
            }
            catch (RfcLogonException e)
            {
                throw e;

                //res.SapErrorMSG = " user could not logon...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                //var master = new Master();
                //master.SapErrorMSG = " user could not logon...";
                //return View("ErrorPage", master);
                // user could not logon...
            }
            catch (RfcAbapRuntimeException e)
            {
                throw e;

                //res.SapErrorMSG = " serious problem on ABAP system side...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                // serious problem on ABAP system side...
                //var master = new Master();
                //master.SapErrorMSG = " serious problem on ABAP system side...";
                //return View("ErrorPage", master);
            }
            catch (RfcAbapBaseException e)
            {
                throw e;

                //var master = new Master();
                //master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                //return View("ErrorPage", master);
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
            }
            catch (Exception e)
            {
                throw e;

                //var master = new Master();
                //master.SapErrorMSG = e.Message;
                //return View("ErrorPage", master);
            }
            finally
            {
                //RfcDestinationManager.UnregisterDestinationConfiguration(config);
            }
            var s = returnMsg.ToString().Trim();
            s = s.ToUpper();
            Username = Username.ToUpper();
            if (s.Equals(Username))

                return true;

            else
            {
                return false;
            }

        }


    }
}