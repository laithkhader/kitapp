﻿using AFOC_APP.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SAP.Middleware.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AFOC_APP.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        public ActionResult Panel()
        {
            return View();
        }
        public ActionResult UsersList()
        {

            var UsersContext = new ApplicationDbContext();
            UsersContext.Users.ToList();

            return View(UsersContext.Users.ToList());
        }
        public ActionResult activateUser(string id)
        {
            var UsersContext = new ApplicationDbContext();
            var user = UsersContext.Users.Find(id);
            user.Status = "Activated";

            UsersContext.Entry(user).State = EntityState.Modified;
            UsersContext.SaveChanges();
            return RedirectToAction("UsersList", UsersContext.Users.ToList());

        }
        public ActionResult deactivateUser(string id)
        {
            var UsersContext = new ApplicationDbContext();
            var user = UsersContext.Users.Find(id);
            user.Status = "NotActivated";

            UsersContext.Entry(user).State = EntityState.Modified;
            UsersContext.SaveChanges();
            return RedirectToAction("UsersList", UsersContext.Users.ToList());

        }
        public ActionResult UsersRoles(string id) {
            Session["UserRoleviewmodel"] = null;
            var context = new ApplicationDbContext();
            var roles=context.Roles.ToList();
            var UserRoleviewmodel = new List<UserRolesViewModel>();


            Session["UserId"] = id;
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(id);
            var v = new UserRolesViewModel();
            int i = 1;
            foreach (var item in s)
            {
                v = new UserRolesViewModel();
                v.ID = i++;
                v.name = item;
                UserRoleviewmodel.Add(v);
            }

            Session["UserRolesViewModel"] = UserRoleviewmodel;

            var rlst = roles.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() , Value = item.Name.ToString().Trim() }).ToList();
            ViewData["Roles"] = rlst;
            return View();



        }
        public ActionResult Authenticate(string id)
        {
            var con = new AFOCDBContext();
            var pr = new PRDBContext();
            var PurchasingGroups = con.PurchasingGroups.ToList();
            var Plants = con.Plants.ToList();
            var MarketListPRs = con.MarketListPRs.ToList();
            var ReleaseCodes =pr.releaseCodes.ToList();
            var CostCenters = con.CostCenters.ToList();


            var rslt = PurchasingGroups.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.name.ToString() }).ToList();
            ViewData["PurchasingGroups"] = rslt;
             rslt = Plants.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
            ViewData["Plants"] = rslt;
             rslt = MarketListPRs.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
            ViewData["MarketListPRs"] = rslt;
            rslt = ReleaseCodes.Select(item => new SelectListItem() { Text = "" + item.Discription.ToString().Trim(), Value = item.Codes.ToString() }).ToList();
            ViewData["ReleaseCodes"] = rslt;
            rslt = CostCenters.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.CostCenterID.ToString() }).ToList();
            ViewData["CostCenters"] = rslt;



            var UREL = pr.user_ReleaseCodes.Where(x => x.UserID == id).ToList();
            var UPG = pr.user_PurchasingGroups.Where(x => x.UserID == id).ToList();
            var UPL = pr.user_Plants.Where(x => x.UserID == id).ToList();
            var UDT = pr.user_MarketListPRs.Where(x => x.UserID == id).ToList();
            var UCC = pr.user_CostCenters.Where(x => x.UserID == id).ToList();

            Session["UREL"] = UREL;
            Session["UPG"] = UPG;
            Session["UPL"] = UPL;
            Session["UDT"] = UDT;
            Session["UCC"] = UCC;

            Session["UserId"] = id;


            return View("Authorize");




        }


        public ActionResult Lines_Read([DataSourceRequest]DataSourceRequest request)
        {
            List<UserRolesViewModel> lst = null;
            var list = new List<UserRolesViewModel>();


            lst = null;
            if (Session["UserRolesViewModel"] == null)
            {
                lst = new List<UserRolesViewModel>();
                Session["UserRolesViewModel"] = lst;

            }


            else
            {
                list = (List<UserRolesViewModel>)Session["UserRolesViewModel"];
            }

            return Json(list.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create([DataSourceRequest]DataSourceRequest request, UserRolesViewModel UR)
        {
            IList<UserRolesViewModel> list = null;
            var u = new UserRolesViewModel();
            if (Session["UserRolesViewModel"] == null)//|| ((List<Line>)Session["PRLine"]).Count == 0)
            {
               
                list.Add(UR);

                Session["UserRolesViewModel"] = list;
            }
            else
            {
                list = (List<UserRolesViewModel>)Session["UserRolesViewModel"];
                var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                var id = (first != null) ? first.ID : 0;
                UR.ID = id + 1;

                
                
                list.Add(UR);
                Session["UserRolesViewModel"] = list;
            }

            ApplicationDbContext context = new ApplicationDbContext();


            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

           var result1= UserManager.AddToRole(Session["UserId"].ToString(), UR.name);



            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update([DataSourceRequest]DataSourceRequest request, UserRolesViewModel UR)
        {
            ApplicationDbContext context = new ApplicationDbContext();
          
            var lst = (List<UserRolesViewModel>)Session["UserRolesViewModel"];
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var r = new IdentityResult();
            foreach (var item in lst)
            {
               r= UserManager.RemoveFromRole(Session["UserId"].ToString(), item.name);
            }
            var i = lst.Where(x => x.ID == UR.ID).First();
            lst.Remove(i);
            lst.Add(UR);


            foreach (var item in lst)
            {
                r=UserManager.AddToRole(Session["UserId"].ToString(), item.name);
            }



            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy([DataSourceRequest]DataSourceRequest request, UserRolesViewModel UR)
        {

           

            if (UR != null)
            {
                List<UserRolesViewModel> lst = null;
                if (Session["UserRolesViewModel"] == null)
                {
                    lst = new List<UserRolesViewModel>();
                    Session["UserRolesViewModel"] = lst;


                }
                else
                {
                    lst = (List<UserRolesViewModel>)Session["UserRolesViewModel"];
                    var pppp = lst.SingleOrDefault(a => a.ID == UR.ID);
                    if (pppp != default(UserRolesViewModel))
                        lst.Remove(pppp);
                    ApplicationDbContext context = new ApplicationDbContext();

                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var r=UserManager.RemoveFromRole(Session["UserId"].ToString(), pppp.name);

                }


            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }
    }
}
