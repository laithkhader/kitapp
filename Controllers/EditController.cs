﻿using AFOC_APP.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using SAP.Middleware.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Net;
using System.Web.Security;

namespace AFOC_APP.Controllers
{
    [Authorize(Roles="PR")]
    public class EditController: Controller 
    {
        
        public ActionResult Edit()
        {
            if (Session["P"] == null)
                 
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");


            if (Request.QueryString.Count != 0)
            {
               return Search(Request.QueryString["DocID"]);

            }
            Session["PRLine"] = null;
            Session["PRLineO"] = null;
            AFOCDBContext Db = new AFOCDBContext();
            PRDBContext con = new PRDBContext();


            var PRDB = new PRDBContext();
            var lst = Db.CostCenters.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() + "||" + item.CostCenterID.ToString(), Value = item.CostCenterID.ToString() }).ToList();
            ViewData["CClist"] = lst;

            var ACC = Db.AccountAssignments.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
            ViewData["ACCList"] = ACC;





            var userID = User.Identity.GetUserId();
            var MLList = PRDB.user_MarketListPRs.Where(x => x.UserID == userID).ToList();
            var ML = MLList.Select(item => new SelectListItem() { Text = "" + item.MasrketListPRDescription.ToString().Trim(), Value = item.MasrketListPRID.ToString() }).ToList();
            ViewData["MLList"] = ML;


            var PList = PRDB.user_Plants.Where(x => x.UserID == userID).ToList();
            var Plant = PList.Select(item => new SelectListItem() { Text = "" + item.PlantDescription.ToString().Trim(), Value = item.PlantID.ToString() }).ToList();
            ViewData["PList"] = Plant;



            var PGList = PRDB.user_PurchasingGroups.Where(x => x.UserID == userID).ToList();
            var PG = PGList.Select(item => new SelectListItem() { Text = "" + item.PurchasingGroupDescription.ToString().Trim(), Value = item.PurchasingGroupID.ToString() }).ToList();
            ViewData["PGList"] = PG;






            var PRs = con.Masters.Select(item => new SelectListItem() { Text = "" + item.DocID.ToString().Trim(), Value = item.DocID.ToString() }).ToList();
            ViewData["PRlist"] = PRs;

            var mlst = Db.Materials.Select(item => new SelectListItem() { Text = "" + item.MaterialDesc.ToString().Trim() + "||" + item.MaterialID.ToString(), Value = item.MaterialID.ToString() }).ToList();
            ViewData["MMList"] = mlst;

            var master = new Master();

            master.DeliveryDateformat = DateTime.Now.Date.AddDays(1);

            return View(master);
        }


        public ActionResult SearchID(string keyword)
        {
            return RedirectToAction("Search", "Edit", Request.QueryString["DocID"]);


        }


        [HttpPost]
        public ActionResult Search(string keyword) {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
            return RedirectToAction("Login", "Account");
            Master res = new Master();
            Session["PRLine"] = null;
            Session["PRLineO"] = null;
            AFOCDBContext Db = new AFOCDBContext();
            var PRDB = new PRDBContext();
            var lst = Db.CostCenters.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() + "||" + item.CostCenterID.ToString(), Value = item.CostCenterID.ToString() }).ToList();
            ViewData["CClist"] = lst;

            var ACC = Db.AccountAssignments.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
            ViewData["ACCList"] = ACC;





            var userID = User.Identity.GetUserId();
            var MLList = PRDB.user_MarketListPRs.Where(x => x.UserID == userID).ToList();
            var ML = MLList.Select(item => new SelectListItem() { Text = "" + item.MasrketListPRDescription.ToString().Trim(), Value = item.MasrketListPRID.ToString() }).ToList();
            ViewData["MLList"] = ML;


            var PList = PRDB.user_Plants.Where(x => x.UserID == userID).ToList();
            var Plant = PList.Select(item => new SelectListItem() { Text = "" + item.PlantDescription.ToString().Trim(), Value = item.PlantID.ToString() }).ToList();
            ViewData["PList"] = Plant;



            var PGList = PRDB.user_PurchasingGroups.Where(x => x.UserID == userID).ToList();
            var PG = PGList.Select(item => new SelectListItem() { Text = "" + item.PurchasingGroupDescription.ToString().Trim(), Value = item.PurchasingGroupID.ToString() }).ToList();
            ViewData["PGList"] = PG;











            var mlst = Db.Materials.Select(item => new SelectListItem() { Text = "" + item.MaterialDesc.ToString().Trim() + "||" + item.MaterialID.ToString(), Value = item.MaterialID.ToString() }).ToList();
            ViewData["MMList"] = mlst;
            if (keyword.Trim() != "")
            {
  
               

                PRDBContext db = new PRDBContext();
                res = db.Masters.Where(f => f.DocID.Contains(keyword)&&f.Creator == System.Web.HttpContext.Current.User.Identity.Name).FirstOrDefault();
                if (res == default(Master))
                {

                    res = new Master();
                    Session["PRLine"] = null;
                    Session["PRLineO"] = null;
                    res.SapErrorMSG = "PR No. Not Found";
                    return View("Edit",res);
                }
                

               

                RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
                ////RfcSessionManager.BeginContext(prd);

                try
                {
                    RfcCustomDestination prd = prdc.CreateCustomDestination();
                    //var user = System.Web.HttpContext.Current.User.Identity.Name;
                    //prd.Password = Session["P"].ToString();
                    //prd.User = user;
                    RfcRepository repo = prd.Repository;
                    IRfcFunction companyBapi = repo.CreateFunction("BAPI_PR_GETDETAIL");
                    IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_GETDETAIL");



                    companyBapi.SetValue("NUMBER", keyword);
                    Requisition.SetValue("NUMBER", keyword);
                    companyBapi.SetValue("ACCOUNT_ASSIGNMENT", "X");
                    companyBapi.SetValue("ITEM_TEXT", "X");
                    companyBapi.SetValue("HEADER_TEXT", "X");
                    companyBapi.Invoke(prd);
                    Requisition.Invoke(prd);


                    IRfcTable REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();

                    IRfcTable PRITEM = companyBapi["PRITEM"].GetTable();
                    IRfcTable PRACCOUNT = companyBapi["PRACCOUNT"].GetTable();
                    IRfcTable PRITEMTEXT = companyBapi["PRITEMTEXT"].GetTable();
                    IRfcTable HeaderText = companyBapi["PRHEADERTEXT"].GetTable();

                    var lineList = new List<Line>();
                    var lineListO = new List<Line>();

                    var line = new Line();

                    string item_NO;
                    int i = 0;
                    foreach (var item in PRITEM)
                    {
                        //if (item.GetString("DELETE_IND") == "X") {
                        //}
                        //else {

                            line = new Line();
                            item_NO = item.GetString("PREQ_ITEM");
                            line.ID = int.Parse(item_NO);

                            line.MaterialID =REQUISITION_ITEMS[i].GetString("MATERIAL");
                            line.Quantity = Decimal.Parse(item.GetString("QUANTITY"));
                            line.Unit = item.GetString("UNIT");
                            line.UnitPrice = Decimal.Parse(item.GetString("PREQ_PRICE"));
                            line.DELETE_IND = item.GetString("DELETE_IND");
                        //line.TotalPrice = decimal.Parse(item.GetString(""));
                        foreach (var cc in PRACCOUNT)
                        {
                            if (item_NO == cc.GetString("PREQ_ITEM"))
                            {
                                line.CostCenterID = cc.GetString("COSTCENTER");

                            }

                        }
                               //line.CostCenterID = PRACCOUNT[i].GetString("COSTCENTER");

                        foreach (var IT in PRITEMTEXT)
                            {
                                if (item_NO == IT.GetString("PREQ_ITEM") && "B01" == IT.GetString("TEXT_ID"))
                                {
                                    line.ItemText = IT.GetString("TEXT_LINE");

                                }
                                if (item_NO == IT.GetString("PREQ_ITEM") && "B04" == IT.GetString("TEXT_ID"))
                                {
                                    line.MaterialPoText = IT.GetString("TEXT_LINE");

                                }

                            }
                            foreach (var header in HeaderText)
                            {
                                if (item_NO == header.GetString("PREQ_ITEM") && "B01" == header.GetString("TEXT_ID"))
                                {
                                    res.HeaderNote = header.GetString("TEXT_LINE");

                                }

                            }
                            lineList.Add(line);
                            lineListO.Add(line);
                            res.PurchasingGroupID = item.GetString("PUR_GROUP");
                            res.MarketListPRID = item.GetString("DOC_TYPE");
                            res.PlantID = item.GetString("PLANT");
                            res.AccountAssignmentID = item.GetString("ACCTASSCAT");
                            res.DeliveryDateformat = DateTime.Parse(item.GetString("DELIV_DATE"));
                        //db.Lines.Attach(line);
                        //db.Entry(line).State = EntityState.Modified;
                        //db.SaveChanges();

                        i++;
                    }


                    Session["PRLine"] = lineList;
                    Session["PRLineO"] = lineListO;



                    IRfcTable RETURN = companyBapi["RETURN"].GetTable();
                    var returnMsg = new System.Text.StringBuilder();
                    
                    foreach (var item in RETURN)
                    {
                        if (item.GetString("TYPE") == "E")
                        {
                            returnMsg.AppendLine(item.GetString("MESSAGE"));
                            returnMsg.AppendLine("");


                        }

                        //if (item.GetString("TYPE") == "S")
                        //{
                        //    if (item[7].GetValue() != null && item[7].GetValue().ToString().Trim() != "")
                        //    {
                        //        returnID = item[7].GetValue().ToString();
                        //        entity.DocID = returnID;

                        //    }

                        //}
                    }

                    
                     if (Session["SAPMSG"] != null)
                    {
                        returnMsg.AppendLine(Session["SAPMSG"].ToString());
                        returnMsg.AppendLine("");
                    }
                    
                    
                    res.SapErrorMSG = returnMsg.ToString();
                    
                    db.Masters.Attach(res);
                    db.Entry(res).State = EntityState.Modified;
                    db.SaveChanges();
    //RfcSessionManager.EndContext(prd);

                }
                catch (RfcCommunicationException e)
                {
                    //pr.Master.SapErrorMSG = " network problem...";
                    //db.Masters.Attach(pr.Master.SapErrorMSG);
                    //db.Entry(pr).State = EntityState.Modified;
                    //db.SaveChanges();
                   var master = new Master();
                    master.SapErrorMSG = " network problem...";
                    return View("ErrorPage", master);
                }
                catch (RfcLogonException e)
                {
                    //res.SapErrorMSG = " user could not logon...";
                    //db.Masters.Attach(res);
                    //db.Entry(res).State = EntityState.Modified;
                    //db.SaveChanges();
                    //return View("Create");

                    var master = new Master();
                    master.SapErrorMSG = " user could not logon...";
                    return View("ErrorPage", master);
                    // user could not logon...
                }
                catch (RfcAbapRuntimeException e)
                {
                    //res.SapErrorMSG = " serious problem on ABAP system side...";
                    //db.Masters.Attach(res);
                    //db.Entry(res).State = EntityState.Modified;
                    //db.SaveChanges();
                    //return View("Create");

                    // serious problem on ABAP system side...
                    var master = new Master();
                    master.SapErrorMSG = " serious problem on ABAP system side...";
                    return View("ErrorPage", master);
                }
                catch (RfcAbapBaseException e)
                {
                    var master = new Master();
                    master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                    return View("ErrorPage", master);
                    // The function module returned an ABAP exception, an ABAP message
                    // or an ABAP class-based exception...
                }
                catch (Exception e)
                {


                    var master = new Master();
                    master.SapErrorMSG = e.Message;
                    return View("ErrorPage", master);
                }
                finally
                {

                }
            }

            return View("Edit",res);

        }
        [HttpPost]
        public ActionResult Update(Master master)
        {

            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            var  db = new PRDBContext();

           var m= db.Masters.FirstOrDefault(e => e.DocID == master.DocID);
            if (m!=null)
            {
                if (!ModelState.IsValid)

                {
                    Session["SAPMSG"] = null;
                    AFOCDBContext Db = new AFOCDBContext();
                    var PRDB = new PRDBContext();
                    var lst = Db.CostCenters.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() + "||" + item.CostCenterID.ToString(), Value = item.CostCenterID.ToString() }).ToList();
                    ViewData["CClist"] = lst;

                    var ACC = Db.AccountAssignments.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
                    ViewData["ACCList"] = ACC;





                    var userID = User.Identity.GetUserId();
                    var MLList = PRDB.user_MarketListPRs.Where(x => x.UserID == userID).ToList();
                    var ML = MLList.Select(item => new SelectListItem() { Text = "" + item.MasrketListPRDescription.ToString().Trim(), Value = item.MasrketListPRID.ToString() }).ToList();
                    ViewData["MLList"] = ML;


                    var PList = PRDB.user_Plants.Where(x => x.UserID == userID).ToList();
                    var Plant = PList.Select(item => new SelectListItem() { Text = "" + item.PlantDescription.ToString().Trim(), Value = item.PlantID.ToString() }).ToList();
                    ViewData["PList"] = Plant;



                    var PGList = PRDB.user_PurchasingGroups.Where(x => x.UserID == userID).ToList();
                    var PG = PGList.Select(item => new SelectListItem() { Text = "" + item.PurchasingGroupDescription.ToString().Trim(), Value = item.PurchasingGroupID.ToString() }).ToList();
                    ViewData["PGList"] = PG;





                    var mlst = Db.Materials.Select(item => new SelectListItem() { Text = "" + item.MaterialDesc.ToString().Trim() + "||" + item.MaterialID.ToString(), Value = item.MaterialID.ToString() }).ToList();
                    ViewData["MMList"] = mlst;

                    master.ErrorMSG = "VError";
                    return View("Edit", master);

                }
                else if (ModelState.IsValid && Session["PRLine"] != null)
                {



                    //ID = lastmaster,
                    m.PlantID = Request.Form["PlantID"];
                    m.AccountAssignmentID = Request.Form["AccountAssignmentID"];
                    m.PurchasingGroupID = Request.Form["PurchasingGroupID"];
                    m.MarketListPRID = Request.Form["MarketListPRID"];
                    m.HeaderNote = Request.Form["HeaderNote"];
                    //DeliveryDate = DateTime.ParseExact(Request.Form["DeliveryDate"].ToString(), "ddMMyyyy",
                    //              CultureInfo.InvariantCulture),
                    m.DeliveryDateformat = DateTime.Parse(Request.Form["DeliveryDateformat"]);
                    m.DeliveryDate = DateTime.Parse(Request.Form["DeliveryDateformat"]).ToString("yyyyMMdd");
                    m.PurchasingTime = DateTime.Now.ToString("HH:mm:ss");


                    var DeliveryDateFormated = m.DeliveryDateformat.ToString("dd.MM.yyyy");
                    db.Masters.Attach(m);
                    db.Entry(m).State = EntityState.Modified;
                    db.SaveChanges();
                    //   var last=db.Masters.Max(p=>p.ID);
                    var lines = (List<Line>)Session["PRLine"];
                    //foreach (var item in lines)
                    //{
                    //    var line = new Line() { MasterID = item.MasterID };
                    //    db.Entry(line).State = EntityState.Deleted;
                    //    db.SaveChanges();

                    //    //item.MasterID = m.ID;
                    //    //db.Lines.Attach(item);
                    //    //db.Entry(item).State = EntityState.Modified;
                    //    //db.SaveChanges();
                    //}
                    //foreach (var item in lines)
                    //{

                    //    item.MasterID = m.ID;
                    //    db.Lines.Add(item);
                    //}
                    //db.SaveChanges();
                    Session["PRID"] = m.ID;




                    RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
                    ////RfcSessionManager.BeginContext(prd);
                    try
                    {

                        RfcCustomDestination prd = prdc.CreateCustomDestination();
                        //var user = System.Web.HttpContext.Current.User.Identity.Name;
                        //prd.Password = Session["P"].ToString();
                        //prd.User = user;

                        RfcRepository repo = prd.Repository;
                        IRfcFunction companyBapi = repo.CreateFunction("BAPI_PR_CHANGE");
                        IRfcFunction transaction = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");
                        transaction.SetValue("WAIT", "X");
                        companyBapi.SetValue("NUMBER", m.DocID);

                        IRfcTable PRITEM = companyBapi["PRITEM"].GetTable();
                        IRfcTable PRITEMX = companyBapi["PRITEMX"].GetTable();
                        IRfcTable PRACCOUNT = companyBapi["PRACCOUNT"].GetTable();
                        IRfcTable PRACCOUNTX = companyBapi["PRACCOUNTX"].GetTable();
                        IRfcTable PRITEMTEXT = companyBapi["PRITEMTEXT"].GetTable();
                        IRfcTable PRHEADERTEXT = companyBapi["PRHEADERTEXT"].GetTable();
                        IRfcStructure header = companyBapi["PRHEADER"].GetStructure();
                        IRfcStructure headerx = companyBapi["PRHEADERX"].GetStructure();
                        header.SetValue("PREQ_NO", m.DocID);

                        header.SetValue("PR_TYPE", m.MarketListPRID);
                        header.SetValue("AUTO_SOURCE", "X");


                        header.SetValue("PREQ_NO", "X");

                        headerx.SetValue("PR_TYPE", "X");
                        headerx.SetValue("AUTO_SOURCE", "X");

                        var PREQ_ITEM = 0;

                        var linesO = (List<Line>)Session["PRLineO"];
                        foreach (var item in linesO)
                        {
                            if (lines.Contains(item))
                            {
                                PRITEM.Append();
                                PRITEMX.Append();
                                PRACCOUNT.Append();
                                PRACCOUNTX.Append();
                                PRITEMTEXT.Append();
                                PRHEADERTEXT.Append();
                                PRITEM.SetValue("PREQ_ITEM", ++PREQ_ITEM);
                                PRITEM.SetValue("MATERIAL", item.MaterialID);
                                PRITEM.SetValue("PREQ_NAME", User.Identity.Name);
                                PRITEM.SetValue("PUR_GROUP", m.PurchasingGroupID);
                                PRITEM.SetValue("PLANT", m.PlantID);
                                PRITEM.SetValue("QUANTITY", item.Quantity);
                                PRITEM.SetValue("ACCTASSCAT", m.AccountAssignmentID);
                                PRITEM.SetValue("DELIV_DATE", DateTime.ParseExact(DeliveryDateFormated, "dd.MM.yyyy", null));

                                PRITEMX.SetValue("PREQ_ITEM", PREQ_ITEM);
                                PRITEMX.SetValue("PREQ_ITEMX", "X");
                                PRITEMX.SetValue("MATERIAL", "X");
                                PRITEMX.SetValue("PREQ_NAME", "X");

                                PRITEMX.SetValue("PUR_GROUP", "X");
                                PRITEMX.SetValue("PLANT", "X");
                                PRITEMX.SetValue("QUANTITY", "X");
                                PRITEMX.SetValue("ACCTASSCAT", "X");
                                PRITEMX.SetValue("DELIV_DATE", "X");

                                PRACCOUNT.SetValue("QUANTITY", item.Quantity);
                                PRACCOUNT.SetValue("SERIAL_NO", 1);
                                PRACCOUNT.SetValue("COSTCENTER", item.CostCenterID.PadLeft(10, '0'));
                                PRACCOUNT.SetValue("PREQ_ITEM", PREQ_ITEM);



                                PRACCOUNTX.SetValue("QUANTITY", "X");
                                PRACCOUNTX.SetValue("SERIAL_NO", 1);
                                PRACCOUNTX.SetValue("COSTCENTER", "X");
                                PRACCOUNTX.SetValue("PREQ_ITEM", PREQ_ITEM);


                                PRITEMTEXT.SetValue("PREQ_ITEM", PREQ_ITEM);
                                PRITEMTEXT.SetValue("TEXT_ID", "B01");
                                PRITEMTEXT.SetValue("TEXT_LINE", item.ItemText);

                                PRITEMTEXT.Append();
                                PRITEMTEXT.SetValue("PREQ_ITEM", PREQ_ITEM);
                                PRITEMTEXT.SetValue("TEXT_ID", "B04");
                                PRITEMTEXT.SetValue("TEXT_LINE", item.MaterialPoText);

                                PRHEADERTEXT.SetValue("TEXT_ID", "B01");
                                PRHEADERTEXT.SetValue("TEXT_LINE", m.HeaderNote);




                            }
                            else
                            {
                                PRITEM.Append();
                                PRITEMX.Append();
                                PRACCOUNT.Append();
                                PRACCOUNTX.Append();
                                PRITEMTEXT.Append();
                                PRHEADERTEXT.Append();
                                PRITEM.SetValue("PREQ_ITEM", ++PREQ_ITEM);
                                PRITEM.SetValue("MATERIAL", item.MaterialID);
                                PRITEM.SetValue("PREQ_NAME", User.Identity.Name);
                                PRITEM.SetValue("PUR_GROUP", m.PurchasingGroupID);
                                PRITEM.SetValue("PLANT", m.PlantID);
                                PRITEM.SetValue("QUANTITY", item.Quantity);
                                PRITEM.SetValue("ACCTASSCAT", m.AccountAssignmentID);
                                PRITEM.SetValue("DELETE_IND", "X");
                                PRITEM.SetValue("DELIV_DATE", DateTime.ParseExact(DeliveryDateFormated, "dd.MM.yyyy", null));

                                PRITEMX.SetValue("PREQ_ITEM", PREQ_ITEM);
                                PRITEMX.SetValue("PREQ_ITEMX", "X");
                                PRITEMX.SetValue("MATERIAL", "X");
                                PRITEMX.SetValue("PREQ_NAME", "X");
                                PRITEMX.SetValue("DELETE_IND", "X");
                                PRITEMX.SetValue("PUR_GROUP", "X");
                                PRITEMX.SetValue("PLANT", "X");
                                PRITEMX.SetValue("QUANTITY", "X");
                                PRITEMX.SetValue("ACCTASSCAT", "X");
                                PRITEMX.SetValue("DELIV_DATE", "X");

                                PRACCOUNT.SetValue("QUANTITY", item.Quantity);
                                PRACCOUNT.SetValue("SERIAL_NO", 1);
                                PRACCOUNT.SetValue("DELETE_IND", "X");
                                PRACCOUNT.SetValue("COSTCENTER", item.CostCenterID.PadLeft(10, '0'));
                                PRACCOUNT.SetValue("PREQ_ITEM", PREQ_ITEM);


                                PRACCOUNTX.SetValue("QUANTITY", "X");
                                PRACCOUNTX.SetValue("DELETE_IND", "X");
                                PRACCOUNTX.SetValue("PREQ_ITEMX", "X");
                                PRACCOUNTX.SetValue("SERIAL_NOX", "X");

                                PRACCOUNTX.SetValue("SERIAL_NO", 1);
                                PRACCOUNTX.SetValue("COSTCENTER", "X");
                                PRACCOUNTX.SetValue("PREQ_ITEM", PREQ_ITEM);


                                PRITEMTEXT.SetValue("PREQ_ITEM", PREQ_ITEM);
                                PRITEMTEXT.SetValue("TEXT_ID", "B01");
                                PRITEMTEXT.SetValue("TEXT_LINE", item.ItemText);

                                PRITEMTEXT.Append();
                                PRITEMTEXT.SetValue("PREQ_ITEM", PREQ_ITEM);
                                PRITEMTEXT.SetValue("TEXT_ID", "B04");
                                PRITEMTEXT.SetValue("TEXT_LINE", item.MaterialPoText);

                                PRHEADERTEXT.SetValue("TEXT_ID", "B01");
                                PRHEADERTEXT.SetValue("TEXT_LINE", m.HeaderNote);


                            }

                        }

                        RfcTransaction trans = new RfcTransaction();
                        RfcTID tid = trans.Tid;

                        try
                        {

                            trans.AddFunction(companyBapi);
                            trans.AddFunction(transaction);
                            trans.Commit(prd);
                        }
                        catch (Exception e)
                        {

                        }
                        // We should execute the Confirm step in the backend only after we are 100%
                        // sure that the data has been deleted on our side, otherwise we risk a
                        // duplicate transaction!

                        prd.ConfirmTransactionID(tid); // This deletes the tid from ARFCRSTATE on
                                                       // backend side.

                        companyBapi.Invoke(prd);


                        IRfcTable RETURN = companyBapi["RETURN"].GetTable();
                        var returnMsg = new System.Text.StringBuilder();

                        var returnID = "";
                        foreach (var item in RETURN)
                        {
                            if (item.GetString("TYPE") == "E")
                            {
                                returnMsg.AppendLine(item.GetString("MESSAGE"));
                                returnMsg.AppendLine("");


                            }

                            if (item.GetString("TYPE") == "S")
                            {
                                if (item[7].GetValue() != null && item[7].GetValue().ToString().Trim() != "")
                                {
                                    returnID = item[7].GetValue().ToString();
                                    m.DocID = returnID;

                                }

                            }
                        }

                        var returnMsg1 = new System.Text.StringBuilder();

                        var lstn = (List<Line>)Session["PRline"];
                        var lsto = (List<Line>)Session["PRlineO"];

                        var result = lsto.Where(p => !lstn.All(p2 => p2.ID == p.ID));
                        if (result != null) {

                            companyBapi = repo.CreateFunction("BAPI_REQUISITION_DELETE");
                            companyBapi.SetValue("NUMBER", master.DocID);
                            IRfcTable REQUISITION_ITEMS_TO_DELETE = companyBapi["REQUISITION_ITEMS_TO_DELETE"].GetTable();
                            transaction = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");
                            transaction.SetValue("WAIT", "X");


                            var REQUISITION_ITEMS_TO_DELETE_LIST = (List<Line>)Session["PRLine"];


                            foreach (var x in result)
                            {
                                REQUISITION_ITEMS_TO_DELETE.Append();

                                REQUISITION_ITEMS_TO_DELETE.SetValue("PREQ_ITEM", x.ID);
                                REQUISITION_ITEMS_TO_DELETE.SetValue("DELETE_IND", "X");

                            }



                            companyBapi.Invoke(prd);


                            transaction.Invoke(prd);
                            RETURN = companyBapi["RETURN"].GetTable();

                            returnID = "";
                            foreach (var item in RETURN)
                            {
                                if (item.GetString("TYPE") == "E")
                                {
                                    returnMsg1.AppendLine(item.GetString("MESSAGE"));
                                    returnMsg1.AppendLine("");


                                }

                            }

                        }


                    


                        if(returnMsg.ToString()!=""|| returnMsg.ToString()=="") { 
                            master.SapErrorMSG = returnMsg.ToString();
                            Session["SAPMSG"]= returnMsg.ToString();
                            if (Session["SAPMSG"].ToString() == "") { Session["SAPMSG"] = null; }
                            return RedirectToAction("Edit", "Edit", master);

                        }
                        else if (returnMsg1.ToString() != "" || returnMsg1.ToString() != null)
                        

                            {
                                master.SapErrorMSG = returnMsg1.ToString();
                            Session["SAPMSG"]= returnMsg.ToString();
                            return RedirectToAction("Edit", "Edit", master);

                        }
                        db.Masters.Attach(m);
                            db.Entry(m).State = EntityState.Modified;
                            db.SaveChanges();
                            //RfcSessionManager.EndContext(prd);

                        }
                    
                    catch (RfcCommunicationException e)
                    {
                        m.SapErrorMSG = " network problem...";
                        db.Masters.Attach(m);
                        db.Entry(m).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (RfcLogonException e)
                    {
                        m.SapErrorMSG = " user could not logon...";
                        db.Masters.Attach(m);
                        db.Entry(m).State = EntityState.Modified;
                        db.SaveChanges();
                        // user could not logon...
                    }

                    catch (RfcAbapRuntimeException e)
                    {
                        m.SapErrorMSG = " serious problem on ABAP system side...";
                        db.Masters.Attach(m);
                        db.Entry(m).State = EntityState.Modified;
                        db.SaveChanges();
                        // serious problem on ABAP system side...
                    }
                    catch (RfcAbapBaseException e)
                    {

                        // The function module returned an ABAP exception, an ABAP message
                        // or an ABAP class-based exception...
                    }
                    finally
                    {
                    }

                }

            }


            
            return RedirectToAction("Edit","Edit");
        }

        public ActionResult Delete(Master master) {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            AFOCDBContext Db = new AFOCDBContext();
            var PRDB = new PRDBContext();
            var lst = Db.CostCenters.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() + "||" + item.CostCenterID.ToString(), Value = item.CostCenterID.ToString() }).ToList();
            ViewData["CClist"] = lst;

            var ACC = Db.AccountAssignments.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
            ViewData["ACCList"] = ACC;





            var userID = User.Identity.GetUserId();
            var MLList = PRDB.user_MarketListPRs.Where(x => x.UserID == userID).ToList();
            var ML = MLList.Select(item => new SelectListItem() { Text = "" + item.MasrketListPRDescription.ToString().Trim(), Value = item.MasrketListPRID.ToString() }).ToList();
            ViewData["MLList"] = ML;


            var PList = PRDB.user_Plants.Where(x => x.UserID == userID).ToList();
            var Plant = PList.Select(item => new SelectListItem() { Text = "" + item.PlantDescription.ToString().Trim(), Value = item.PlantID.ToString() }).ToList();
            ViewData["PList"] = Plant;



            var PGList = PRDB.user_PurchasingGroups.Where(x => x.UserID == userID).ToList();
            var PG = PGList.Select(item => new SelectListItem() { Text = "" + item.PurchasingGroupDescription.ToString().Trim(), Value = item.PurchasingGroupID.ToString() }).ToList();
            ViewData["PGList"] = PG;



            var mlst = Db.Materials.Select(item => new SelectListItem() { Text = "" + item.MaterialDesc.ToString().Trim() + "||" + item.MaterialID.ToString(), Value = item.MaterialID.ToString() }).ToList();
            ViewData["MMList"] = mlst;

            RfcDestination prdc= RfcDestinationManager.GetDestination("AFOCH");
            ////RfcSessionManager.BeginContext(prd);
            try
            {
                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //var user = System.Web.HttpContext.Current.User.Identity.Name;
                //prd.Password = Session["P"].ToString();
                //prd.User = user;
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("BAPI_REQUISITION_DELETE");
                companyBapi.SetValue("NUMBER",master.DocID);
                IRfcTable REQUISITION_ITEMS_TO_DELETE = companyBapi["REQUISITION_ITEMS_TO_DELETE"].GetTable();
                IRfcFunction transaction = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");
                transaction.SetValue("WAIT", "X");


                var REQUISITION_ITEMS_TO_DELETE_LIST = (List<Line>)Session["PRLine"];
                int PREQ_ITEM=0;


                    foreach (var x in REQUISITION_ITEMS_TO_DELETE_LIST)
                     {
                    REQUISITION_ITEMS_TO_DELETE.Append();

                    REQUISITION_ITEMS_TO_DELETE.SetValue("PREQ_ITEM", ++PREQ_ITEM);
                    REQUISITION_ITEMS_TO_DELETE.SetValue("DELETE_IND", "X");

                    }
                   

                
                companyBapi.Invoke(prd);

                PREQ_ITEM = 0;

                transaction.Invoke(prd);
                IRfcTable RETURN = companyBapi["RETURN"].GetTable();
                var returnMsg = new System.Text.StringBuilder();

                var returnID = "";
                foreach (var item in RETURN)
                {
                    if (item.GetString("TYPE") == "E")
                    {
                        returnMsg.AppendLine(item.GetString("MESSAGE"));
                        returnMsg.AppendLine("");


                    }
                    master = new Master();
                    master.SapErrorMSG = returnMsg.ToString();

                    //if (item.GetString("TYPE") == "S")
                    //{
                    //    if (item[7].GetValue() != null && item[7].GetValue().ToString().Trim() != "")
                    //    {
                    //        returnID = item[7].GetValue().ToString();
                    //        master.DocID = returnID;

                    //    }

                    //}
                }


//RfcSessionManager.EndContext(prd);



            }
            catch (Exception)
            {

                master.SapErrorMSG = "Exeption";

            }
            finally
            {

            }


            return RedirectToAction("Edit", "Edit");
        }

        private PRDBContext db = new PRDBContext();
        public ActionResult Lines_Read([DataSourceRequest]DataSourceRequest request, int MasterID)
        {
            List<Line> lst = null;
            var list = new List<Line>();

            if (MasterID == 0)
            {
                if (Session["PRLine"] == null)
                {
                    lst = new List<Line>();
                    Session["PRLine"] = lst;
                }
                else
                {
                    lst = (List<Line>)Session["PRLine"];

                }
                foreach (var item in lst)
                {
                    list.Add(item);
                }
            }
            else
            {
                PRDBContext entities = new PRDBContext();

                IList<Line> result = new List<Line>();

                //list = entities.Lines.Where(p => p.MasterID == MasterID).ToList().Select(row => new Line
                //{
                //    ID = row.ID,
                //    MaterialID = row.MaterialID,
                //    CostCenterID = row.CostCenterID,
                //    Quantity = row.Quantity,
                //    UnitPrice = row.UnitPrice,
                //    Unit = row.Unit,
                //    ItemText = row.ItemText,
                //    MaterialPoText = row.MaterialPoText,
                //    TotalPrice = row.TotalPrice,



                //}

                //).ToList();
                list= (List<Line>)Session["PRLine"];
                lst = null;
                if (Session["PRLine"] == null)
                {
                    lst = new List<Line>();
                    Session["PRLine"] = lst;


                }
                //else
                //{
                //    lst = (List<Line>)Session["PRLine"];
                //    lst.Clear();
                //}
                //foreach (var item in list)
                //{
                //    lst.Add(item);
                //}
            }

            return Json(list.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create([DataSourceRequest]DataSourceRequest request, Line line)
        {
            AFOCDBContext Db = new AFOCDBContext();
            var cc = Db.CostCenters.SingleOrDefault(a => a.CostCenterID == line.CostCenterID);
            var M = Db.Materials.SingleOrDefault(a => a.MaterialID == line.MaterialID);

            if (ModelState.IsValid && line != null)
            {
                IList<Line> list = null;
                if (Session["PRLine"] == null)//|| ((List<Line>)Session["PRLine"]).Count == 0)
                {
                    list = new List<Line>();

                    line.ID = 1;
                    line.MatDesc = M.MaterialDesc;
                    line.Unit = M.UnitType;
                    line.UnitPrice = M.UnitPrice;
                    line.CostCenterName = cc.Name;
                    line.CostCenterDescription = cc.Description;
                    list.Add(line);
                    Session["PRLine"] = list;
                }
                else
                {
                    list = (List<Line>)Session["PRLine"];
                    var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                    var id = (first != null) ? first.ID : 0;
                    line.ID = id + 1;

                    line.MatDesc = M.MaterialDesc;
                    line.Unit = M.UnitType;
                    line.UnitPrice = M.UnitPrice;
                    line.CostCenterName = cc.Name;
                    line.CostCenterDescription = cc.Description;
                    list.Add(line);
                    Session["PRLine"] = list;
                }
            }

            return Json(new[] { line }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update([DataSourceRequest]DataSourceRequest request, Line line)
        {
            if (line.DELETE_IND == "X" || line.DELETE_IND=="x") {
                return Json(new[] { line }.ToDataSourceResult(request, ModelState));
            }

            AFOCDBContext Db = new AFOCDBContext();
            var cc = Db.CostCenters.SingleOrDefault(a => a.CostCenterID == line.CostCenterID);
            var M = Db.Materials.SingleOrDefault(a => a.MaterialID == line.MaterialID);

            IList<Line> list = null;
            if (ModelState.IsValid)
            {
                list = (List<Line>)Session["PRLine"];
                line.MaterialID = M.MaterialID;
                line.MatDesc = M.MaterialDesc;
                line.CostCenterName = cc.Name;
                line.CostCenterDescription = cc.Description;
                foreach (var item in list)
                {
                    if (line.ID == item.ID)
                    {
                        item.ItemText = line.ItemText;

                        item.MaterialID = line.MaterialID;
                        item.MatDesc = line.MatDesc;
                        item.CostCenterName = line.CostCenterName;
                        item.CostCenterDescription = line.CostCenterDescription;

                        item.Quantity = line.Quantity;
                        item.Unit = line.Unit;
                        item.UnitPrice = line.UnitPrice;
                        item.TotalPrice = line.TotalPrice;
                        item.CostCenterID = line.CostCenterID;
                        item.MaterialPoText = line.MaterialPoText;
                        break;
                    }

                }
                //Session["PRLine"] = list;
                //var entity = new Line
                //{
                //    ID = line.ID,
                //    MatDesc = line.MatDesc,
                //    Quantity = line.Quantity,
                //    Unit = line.Unit,
                //    UnitPrice = line.UnitPrice,
                //    TotalPrice = line.TotalPrice,
                //    CostCenterName = line.CostCenterName,
                //    CostCenterDescription = line.CostCenterDescription,
                //    ItemText = line.ItemText,
                //    MaterialPoText = line.MaterialPoText,
                //};


                //db.Lines.Attach(entity);
                //db.Entry(entity).State = EntityState.Modified;
                //db.SaveChanges();
            }

            return Json(new[] { line }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy([DataSourceRequest]DataSourceRequest request, Line line)

        {
            if (line.DELETE_IND == "X" || line.DELETE_IND == "x")
            {
                return Json(new[] { line }.ToDataSourceResult(request, ModelState));
            }

            if (line != null)
            {
                List<Line> lst = null;
                if (Session["PRLine"] == null)
                {
                    lst = new List<Line>();
                    Session["line"] = lst;
                   

                }
                else
                {
                    lst = (List<Line>)Session["PRLine"];
                    var pppp = lst.SingleOrDefault(a => a.ID == line.ID);
                    if (pppp != default(Line))
                        lst.Remove(pppp);

                }
            }
            
       

            return Json(new[] { line }.ToDataSourceResult(request, ModelState));
        }


        public JsonResult GetMaterial(string ID)
        {
            AFOCDBContext Db = new AFOCDBContext();
            var data = Db.Materials.FirstOrDefault(m => m.MaterialID == ID);
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public JsonResult PRs(string ID)
        {
            PRDBContext Db = new PRDBContext();
            var data = Db.Masters.FirstOrDefault(m => m.DocID == ID);
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}

