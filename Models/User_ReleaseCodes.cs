﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class User_ReleaseCodes
    {
        public int ID { get; set; }

        public string UserID { get; set; }
        public string ReleaseCodeID { get; set; }
        public string ReleaseCodeDescription { get; set;}

    }
}