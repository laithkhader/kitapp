﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class DOC_MAT_TYPE
    {
        public int ID { get; set; }
        public string MarketListPRID { get; set; }
        public string MAT_TYPE { get; set; }
    }
}