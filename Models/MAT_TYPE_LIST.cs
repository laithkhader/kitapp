﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class MAT_TYPE_LIST
    {
        public int ID { get; set; }
        public string MaterialID { get; set; }
        public string MaterialType { get; set; }
        public string Sourced { get; set; }
        public string PLANTID { get; set; }
    }
}