﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class MAT_PLANT
    {
        public int ID { get; set; }
        public string MaterialID { get; set; }
        public string PlantID { get; set; }
    }
}