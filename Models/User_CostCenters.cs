﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class User_CostCenters
    {
        public int ID { get; set; }

        public string UserID { get; set; }
        public string CostCenterID { get; set; }
        public string CostCenterDescription { get; set; }
    }
}