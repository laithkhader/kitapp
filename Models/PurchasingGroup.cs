﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class PurchasingGroup
    {
        [Key]
        
        public string name { get; set; }
        [Display(Name = "PurchasingGroup Desc")]
        public string Description { get; set; }
    }
}