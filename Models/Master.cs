﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class Master
    {
      //  [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Display(Name = "PR Number")]

        [StringLength(10, ErrorMessage = "Max of 10 Characters")]
        public string DocID { get; set; }

        public string Creator { get; set; }
        public string Release_code { get; set; }



        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        //[Display(Name = "Delivery Date")]
        //[Required]

        public string DeliveryDate { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Delivery Date")]
        [Required]

        public DateTime DeliveryDateformat { get; set; }

        [StringLength(500, ErrorMessage = "Max of 500 Characters")]
        [DataType(DataType.MultilineText)]
        public string HeaderNote { get; set; }


        public string PurchasingTime { get; set; }

        [Required]
        [StringLength(4, ErrorMessage = "Max of 4 Characters")]
        [Display(Name = "Plant")]

        //hasdefaultvalue
        public string PlantID { get; set; }

        [Required]
        [StringLength(4, ErrorMessage = "Max of 4 Characters")]
        [Display(Name = "PR Doc Type")]

        //hasdefaultvalue
        public string MarketListPRID { get; set; }

        [Required]
        [StringLength(1, ErrorMessage = "Max of 1 Characters")]
        [Display(Name = "Account Assignment")]
        public string AccountAssignmentID  { get; set; }

        [Required]
        [StringLength(3, ErrorMessage = "Max of 3 Characters")]
        [Display(Name = "Purchasing Group")]
        public string PurchasingGroupID { get; set; }

        public string ErrorMSG { get; set; }
        public string SapErrorMSG { get; set; }







        public virtual ICollection<Line> Lines { get; set; }
        public virtual AccountAssignment AccountAssignment { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual PurchasingGroup PurchasingGroup { get; set; }
        public virtual MarketListPR MarketListPR { get; set; }

       

    }

}