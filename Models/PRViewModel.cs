﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class PRViewModel
    {
        [Key]
        public string DocID { get; set; }

        public string RepID { get; set; }

        public Master Master { get; set; }
        public Line Line { get; set; }
        public string Plant_ID { get; set; }
        public string Plant_Description { get; set; }
        [Display(Name = "PREQ_DATE")]
        public string REL_DATE { get; set; }
        public string REL_Code { get; set; }
        [Display(Name = "Total")]

        public decimal PR_Total_price { get; set; }


        public string PG_ID { get; set; }
        public string PG_Description { get; set; }
        [Display(Name = "Material Group")]
        public string MAT_GRP { get; set; }
        public bool Checked { get; set; }
        public PRViewModel()
        {
            Master = new Master();
            Line = new Line();
            Checked = false;
        }
    }


}