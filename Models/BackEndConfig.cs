﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAP.Middleware.Connector;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Security;
using AFOC_APP.Models;
using Microsoft.AspNet.Identity.Owin;

namespace AFOC_APP.Models
{
    public class BackEndConfig : IDestinationConfiguration
    {
        public BackEndConfig(string Username, string Password)
        {
            this.Password = Password;
            this.Username = Username;
        }
        public BackEndConfig(string Password)
        {
            this.Password = Password;
            this.Username = Username;
        }
        public BackEndConfig()
        {


        }


        public string Password { get; set; }
        public string Username { get; set; }
        public RfcConfigParameters GetParameters(String destinationName)
        {

            var context = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();


            if ("AFOCH".Equals(destinationName))
            {
                RfcConfigParameters parms = new RfcConfigParameters();



                parms.Add(RfcConfigParameters.AppServerHost, System.Configuration.ConfigurationManager.AppSettings["SAPIPDEV"].ToString());
                //parms.Add(RfcConfigParameters.MessageServerHost, "hec01psd05.injcldhec.ae");
                // parms.Add(RfcConfigParameters.MessageServerService, "3600");
                //  parms.Add(RfcConfigParameters.LogonGroup, "PUBLIC");
                // parms.Add(RfcConfigParameters.SystemID, "S4D");
                parms.Add(RfcConfigParameters.SystemNumber, System.Configuration.ConfigurationManager.AppSettings["SystemNumber"].ToString());
                parms.Add(RfcConfigParameters.Client, System.Configuration.ConfigurationManager.AppSettings["Client"].ToString());
                parms.Add(RfcConfigParameters.Language, System.Configuration.ConfigurationManager.AppSettings["Language"].ToString());
                parms.Add(RfcConfigParameters.PoolSize, System.Configuration.ConfigurationManager.AppSettings["PoolSize"].ToString());
                parms.Add(RfcConfigParameters.MaxPoolSize, System.Configuration.ConfigurationManager.AppSettings["MaxPoolSize"].ToString());
                parms.Add(RfcConfigParameters.IdleTimeout, System.Configuration.ConfigurationManager.AppSettings["IdleTimeout"].ToString());
                parms.Add(RfcConfigParameters.User, "RFC_USER1");

                parms.Add(RfcConfigParameters.Password, "afoc1234");
                // if (Username != null)
                // {
                //     parms.Add(RfcConfigParameters.User,Username);

                //}
                // else {
                //     parms.Add(RfcConfigParameters.User, HttpContext.Current.User.Identity.Name);
                // }

                // if (Password == null)
                // {
                //     parms.Add(RfcConfigParameters.Password, "");

                // }
                // else {
                //     parms.Add(RfcConfigParameters.Password, Password);
                // }

                return parms;
            }
            else return null;
        }
        // The following two are not used in this example:         
        public bool ChangeEventsSupported()
        { return false; }


        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;
    }

    //public class ServerConfig : IServerConfiguration
    //{
    //    public RfcConfigParameters GetParameters(String serverName)
    //    {
    //        if ("PRD_REG_SERVER".Equals(serverName))
    //        {
    //            RfcConfigParameters parms = new RfcConfigParameters();
    //            parms.Add(RfcConfigParameters.RepositoryDestination, "PRD_000");
    //            parms.Add(RfcConfigParameters.GatewayHost, "demoserver");
    //            parms.Add(RfcConfigParameters.GatewayService, "sapgw00");
    //            parms.Add(RfcConfigParameters.ProgramID, "DEMO_SERVER");
    //            parms.Add(RfcConfigParameters.RegistrationCount, "5");
    //            return parms;
    //        }
    //        else return null;
    //    }         // The following code is not used in this example         
    //    public bool ChangeEventsSupported() { return false; }
    //    public event RfcServerManager.ConfigurationChangeHandler ConfigurationChanged;
    //}

    //public class ServerHandler
    //{
    //    [RfcServerFunction(Name = "STFC_CONNECTION")]
    //    public static void StfcConnection(RfcServerContext context, IRfcFunction function)
    //    {
    //        Console.WriteLine("Received function call { 0} from system { 1}.",
    //        function.Metadata.Name, context.SystemAttributes.SystemID);
    //        String reqtext = function.GetString("REQUTEXT");
    //        Console.WriteLine("REQUTEXT = { 0}\n", reqtext);
    //        function.SetValue("ECHOTEXT", reqtext);
    //        function.SetValue("RESPTEXT", "Hello from NCo 3.0!");
    //    }
    //}
}


