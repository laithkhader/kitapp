﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace AFOC_APP.Models
{
    public class AFOCDBContext : DbContext
    {
        public AFOCDBContext() : base("AFOCDBContext")
        {
            Database.SetInitializer<AFOCDBContext>(new AFOCDBInitializer());//new DropCreateDatabaseIfModelChanges<AFOCDBContext>());

        }

        public DbSet<CostCenter> CostCenters { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<PurchasingGroup> PurchasingGroups { get; set; }
        public DbSet<MarketListPR> MarketListPRs { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<AccountAssignment> AccountAssignments { get; set; }


        public DbSet<DOC_MAT_TYPE> DOC_MAT_TYPE { get; set; }
        public DbSet<MAT_PLANT> MAT_PLANT { get; set; }
        public DbSet<MAT_TYPE_LIST> MAT_TYPE_LIST { get; set; }


    }
}