﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class Plant
    {
        [Key]
        public string Type { get; set; }
        [Display(Name ="Plant")]
        public string Description { get; set; }
    }
}