﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class CostCenter
    {
        [Key]
        public String CostCenterID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
    }
}