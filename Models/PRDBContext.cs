﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace AFOC_APP.Models
{
    public class PRDBContext : DbContext
    {
        public PRDBContext() : base("PRDBContext")
        {
            Database.SetInitializer<PRDBContext>(new DropCreateDatabaseIfModelChanges<PRDBContext>());

        }
        public DbSet<Line> Lines { get; set; }

        public System.Data.Entity.DbSet<AFOC_APP.Models.CostCenter> CostCenters { get; set; }

        public System.Data.Entity.DbSet<AFOC_APP.Models.Master> Masters { get; set; }

        public System.Data.Entity.DbSet<AFOC_APP.Models.Material> Materials { get; set; }
        public System.Data.Entity.DbSet<AFOC_APP.Models.User_MarketListPR> user_MarketListPRs { get; set; }
        public System.Data.Entity.DbSet<AFOC_APP.Models.User_Plant> user_Plants { get; set; }
        public System.Data.Entity.DbSet<AFOC_APP.Models.User_PurchasingGroups> user_PurchasingGroups { get; set; }
        public System.Data.Entity.DbSet<AFOC_APP.Models.User_ReleaseCodes> user_ReleaseCodes { get; set; }
        public System.Data.Entity.DbSet<AFOC_APP.Models.User_CostCenters> user_CostCenters { get; set; }
        public System.Data.Entity.DbSet<AFOC_APP.Models.ReleaseCodes> releaseCodes { get; set; }


    }
}